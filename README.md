# VideoLAN armv7 docker images

VideoLAN armv7 docker images are used in GitLab CI jobs to provide static build environments.

On push, the [GitLab CI jobs] will launch and figure out the images that need to be rebuilt.

The results will be available on VideoLAN docker registry under registry.videolan.org:5000/$tag.

The suffix of -armv7 will be appended when making an image name prior to
push.  I know it's horrible and hacky, but it's a good way to ensure we can
distinguish between amd64, aarch64 and armv7 images.

Also, make sure to use USER at the end of Dockerfile to ensure we're not running builds as root.

   [Gitlab CI jobs]: <https://code.videolan.org/videolan/docker-images-armv7/-/jobs>

